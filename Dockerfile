ARG ALPINE_VERSION=latest
ARG CRYSTAL_VERSION
ARG SHARDS_VERSION

FROM alpine:latest AS fetch
RUN sed -i -e 's/v[[:digit:]]\.[[:digit:]]\+/edge/g' /etc/apk/repositories && \
	apk upgrade --update-cache --available

ARG CRYSTAL_VERSION
RUN apk update && \
	apk add crystal$([ ! -z "$CRYSTAL_VERSION" ] && echo "=${CRYSTAL_VERSION}")

ARG SHARDS_VERSION
RUN apk update && \
	apk add shards$([ ! -z "$SHARDS_VERSION" ] && echo "=${SHARDS_VERSION}")

FROM alpine:${ALPINE_VERSION} AS main
RUN apk add --update \
	alpine-sdk \
	gc-dev \
	gcc \
	git \
	gmp-dev \
	libatomic_ops \
	libevent \
	libevent-dev \
	libevent-static \
	libgcc \
	libressl-dev \
	libstdc++ \
	libxml2-dev \
	llvm5 \
	llvm5-dev \
	llvm5-libs \
	llvm5-static \
	musl \
	musl-dev \
	openssl-dev \
	pcre \
	pcre-dev \
	readline-dev \
	yaml-dev \
	zlib-dev

COPY --from=fetch /usr/bin/crystal /usr/bin/crystal
COPY --from=fetch /usr/lib/crystal /usr/lib/crystal
COPY --from=fetch /usr/bin/shards /usr/bin/shards
